
CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Installation
 * Credits
 
 
INTRODUCTION
------------
CAS Register by Invite ( http://drupal.org/project/cas_register_invite ) allows a Drupal instance to restrict registration to those who can successfully authenticate via CAS. This module does not interface with Drupal's user authentication system. Instead, it sends an invitation via email to register on the Drupal site (like normal) to users who successfully authenticate via CAS. In this way, CAS is used as part of the registration process, but not to log in.

This module is incompatible with the following modules: cas, invite.


INSTALLATION (NON-STANDARD!)
------------
1. Ensure that your version of PHP has the Curl, OpenSSL, DOM, and zlib modules enabled:
  --with-curl
  --with-openssl
  --with-dom
  --with-zlib

2. Copy the entire cas_register_invite project directory (not just the contents) to your normal module directory (ie: sites/all/modules)

3. Ensure that the phpCAS library (version >=1.0.1) is on your path. There are two ways to do this:
	A. Download and extract the phpCAS library ( http://www.ja-sig.org/wiki/display/CASC/phpCAS+installation+guide ), copy the "CAS-1.x.x" directory (not just the contents) into this module's directory (cas_register_invite) and rename it to "CAS" (you should have a directory "modules/cas_register_invite/CAS/CAS"); or
	B. Install with PEAR (pear install http://www.ja-sig.org/downloads/cas-clients/php/1.0.1/CAS-1.0.1.tgz ). This assumes that PEAR is on your path.

4. Enable the CAS Register by Invite module at ?q=admin/build/modules

5. Visit the CAS Register by Invite settings page at ?q=admin/user/cas-register-invite


CREDITS
-------
Developer and maintainer: JD Leonard ( http://drupal.org/user/80902 )

CAS Register by Invite is based on code from the CAS ( http://drupal.org/project/cas ) and Invite ( http://drupal.org/project/invite ) modules.
